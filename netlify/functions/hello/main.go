package main

import (
	"context"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"gitlab.com/hesperos/lambda/pkg/hackernews"
)

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	hn := hackernews.NewClient(hackernews.BaseUrl)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	topStories, err := hn.TopStories(ctx)
	if err != nil {
	}

	builder := &strings.Builder{}
	builder.WriteString("Hello from lambda.  Here are some HN titles\n\n")

	maxItems := 100
	maxParallel := 16

	storyChan := make(chan *hackernews.Story)
	semaChan := make(chan bool, maxParallel)
	wg := sync.WaitGroup{}

	wg.Add(maxItems)
	for _, storyId := range topStories.Stories[:maxItems] {
		go func(storyId uint64) {
			semaChan <- true
			defer func() {
				<-semaChan
				wg.Done()
			}()
			story, err := hn.StoryById(ctx, storyId)
			if err != nil {
			}
			storyChan <- story
		}(storyId)
	}

	go func() {
		wg.Wait()
		close(storyChan)
	}()

	for story := range storyChan {
		builder.WriteString(story.Title)
		builder.WriteRune('\n')
	}

	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       builder.String(),
	}, nil
}

func main() {
	lambda.Start(handler)
}
