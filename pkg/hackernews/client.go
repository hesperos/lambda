package hackernews

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"path"
	"time"
)

var (
	BaseUrl string = "https://hacker-news.firebaseio.com/v0"
)

type Client struct {
	baseUrl string
	client  *http.Client
}

type Story struct {
	By          string `json:"by"`
	Descendants int    `json:"descendants"`
	Id          int    `json:"id"`
	Kids        []int  `json:"kids"`
	Score       int    `json:"score"`
	Time        uint64 `json:"time"`
	Title       string `json:"title"`
	Url         string `json:"url"`
}

type TopStories struct {
	Stories []uint64
}

func (c *Client) makeUrl(endpoint string) (*url.URL, error) {
	u, err := url.Parse(c.baseUrl)
	if err != nil {
		return nil, err
	}

	u.Path = path.Join(u.Path, endpoint)
	return u, nil
}

func (c *Client) doGet(ctx context.Context, u *url.URL, value interface{}) error {
	req, err := http.NewRequestWithContext(ctx, "GET", u.String(), nil)
	if err != nil {
		return err
	}

	req.Header.Set("Accept", "application/json")

	log.Println(u.String())
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < http.StatusOK && resp.StatusCode >= http.StatusBadRequest {
		return errors.New("Resource unavailable")
	}

	log.Println("Got status:", resp.Status)
	if err := json.NewDecoder(resp.Body).Decode(value); err != nil {
		return err
	}

	// drain the remainder
	io.Copy(ioutil.Discard, resp.Body)
	return nil
}

func (c *Client) StoryById(ctx context.Context, storyId uint64) (*Story, error) {
	documentId := fmt.Sprintf("/item/%d.json", storyId)
	u, err := c.makeUrl(documentId)
	if err != nil {
		return nil, err
	}

	story := &Story{}
	if err := c.doGet(ctx, u, story); err != nil {
		return nil, err
	}
	return story, nil
}

func (c *Client) TopStories(ctx context.Context) (*TopStories, error) {
	u, err := c.makeUrl("topstories.json")
	if err != nil {
		return nil, err
	}

	tp := &TopStories{}
	if err := c.doGet(ctx, u, &tp.Stories); err != nil {
		return nil, err
	}

	return tp, nil
}

func NewClient(baseUrl string) *Client {
	c := &Client{
		baseUrl: baseUrl,
		client: &http.Client{
			Transport: &http.Transport{
				MaxIdleConnsPerHost: 32,
				TLSHandshakeTimeout: 20 * time.Second,
			},
			Timeout: 10 * time.Second,
		},
	}
	return c
}
