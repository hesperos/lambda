package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"sync"
	"time"

	"gitlab.com/hesperos/lambda/pkg/hackernews"
)

func main() {
	log.SetOutput(io.Discard)
	// f, err := os.Create("cpuprofile.pprof")
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// pprof.StartCPUProfile(f)
	// defer pprof.StopCPUProfile()

	hn := hackernews.NewClient(hackernews.BaseUrl)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	// clientTrace := &httptrace.ClientTrace{
	// 	GotConn: func(info httptrace.GotConnInfo) {
	// 		log.Println(info.Reused)
	// 	},
	// }
	// ctx = httptrace.WithClientTrace(ctx, clientTrace)

	stories := []*hackernews.Story{}
	topStories, err := hn.TopStories(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	storyChan := make(chan *hackernews.Story)
	semaChan := make(chan bool, 8)
	wg := sync.WaitGroup{}

	maxItems := 10
	wg.Add(maxItems)
	for _, storyId := range topStories.Stories[:maxItems] {
		go func(storyId uint64) {
			semaChan <- true
			defer func() {
				<-semaChan
				wg.Done()
			}()
			story, err := hn.StoryById(ctx, storyId)
			if err != nil {
				log.Fatalln(err)
			}
			storyChan <- story
		}(storyId)
	}

	go func() {
		wg.Wait()
		close(storyChan)
	}()

	for story := range storyChan {
		stories = append(stories, story)
	}

	for _, s := range stories {
		fmt.Println(s.Title)
	}
}
